use std::env;
use std::path::PathBuf;

/// Usage
///
/// fdm [flags] pattern [path]
///
/// Flags:
///     -H, --hidden    include hidden files/folders

fn main() -> Result<()> {
    let opts = Opt::from_args()?;
    dbg!(&opts);
    run(opts)?;

    Ok(())
}

fn run(opts: Opt) -> Result<()> {
    for path in opts.paths {
        walk_dir(path, &opts.pattern)?;
    }

    Ok(())
}

fn walk_dir(path: PathBuf, pattern: &String) -> Result<()> {
    for entry in path.read_dir()? {
        let entry = entry?;
        let name = entry.file_name().into_string()?;
        if name.contains(pattern) {
            let entry_path = entry.path();
            let path_str = entry_path.to_str().unwrap();
            println!("{:?}", path_str);
        };
        if entry.file_type()?.is_dir() {
            walk_dir(entry.path(), pattern)?;
        }
    }

    Ok(())
}

type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug)]
enum Error {
    IoError(std::io::Error),
    InvalidArgumentError(String),
    ArgumentError(String),
    NonUTF8Name(String),
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self {
        Error::IoError(err)
    }
}

impl From<std::ffi::OsString> for Error {
    fn from(name: std::ffi::OsString) -> Self {
        Error::NonUTF8Name(format!(
            "Non UTF-8 name found '{}'",
            name.to_string_lossy().into_owned(),
        ))
    }
}

/// Options for the program, prepared after parsing arguments
#[derive(Debug, Default)]
struct Opt {
    /// include hidden files/folders?
    hidden: bool,

    /// search pattern
    pattern: String,

    /// root search paths
    paths: Vec<PathBuf>,
}

impl Opt {
    fn from_args() -> Result<Opt> {
        let raw_args: Vec<String> = env::args().collect();

        let mut opt = Opt::default();
        let mut pattern_set = false;

        for arg in raw_args.iter().skip(1) {
            if arg == "-H" || arg == "--hidden" {
                opt.hidden = true;
            } else if arg.starts_with("-") {
                return Err(Error::InvalidArgumentError(format!(
                    "unknown flag '{}'",
                    arg
                )));
            } else if !pattern_set {
                opt.pattern = arg.clone();
                pattern_set = true;
            } else {
                opt.paths.push(PathBuf::from(arg));
            }
        }

        if !pattern_set {
            return Err(Error::ArgumentError(
                "atleast one pattern is required, none was provided.".to_string(),
            ));
        }

        if opt.paths.is_empty() {
            opt.paths.push(env::current_dir()?);
        }

        Ok(opt)
    }
}
